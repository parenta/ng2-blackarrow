/**
 * Created by ADavis on 06/09/2016.
 */
import { AuthenticationService } from '@parenta/ng2-sauron';
export declare class AuthenticationProxyService {
    private authenticationService;
    constructor(authenticationService: AuthenticationService);
    sauronKey(): string;
}
