import { IMail } from './entities/mail';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { AuthenticationProxyService } from './authentication-proxy.service';
import { IBlackArrowClientConfig } from './config/iblack-arrow-client-config';
export declare class MailService {
    private config;
    private http;
    private authenticationProxyService;
    constructor(config: IBlackArrowClientConfig, http: Http, authenticationProxyService: AuthenticationProxyService);
    sendMail(mail: IMail): Observable<string>;
}
