import ConnectionOptions = SignalR.ConnectionOptions;
import StateChanged = SignalR.StateChanged;
import ConnectionError = SignalR.ConnectionError;
import Transport = SignalR.Transport;
export class MessengingHub {

    connection: SignalR.Hub.Connection;
    proxy: SignalR.Hub.Proxy;

    private getConnection(options: IHubOptions): SignalR.Hub.Connection {
        let connection = null;
        if (options && options.rootPath) {
            connection = this.$.hubConnection(options.rootPath, { useDefaultPath: false });
        } else {
            connection = this.$.hubConnection();
        }

        connection.logging = (options && options.logging ? true : false);
        return connection;
    }

    constructor(hubName: string, private options: IHubOptions, private $: JQueryStatic) {
        this.connection = this.getConnection(options);
        this.proxy = this.connection.createHubProxy(hubName);

        if (options && options.listeners) {
            Object.getOwnPropertyNames(options.listeners)
                .filter(function (propName) {
                    return typeof options.listeners[propName] === 'function';
                })
                .forEach(function (propName) {
                    this.on(propName, options.listeners[propName]);
                });
        }
        if (options && options.queryParams) {
            this.connection.qs = options.queryParams;
        }
        if (options && options.errorHandler) {
            this.connection.error(options.errorHandler);
        }
        if (options && options.stateChanged) {
            this.connection.stateChanged(options.stateChanged);
        }
    }

    on(eventName: string, callback: (...msg: any[]) => void) {
        this.proxy.on(eventName, callback);
    };
    invoke = function (method, args) {
        return this.proxy.invoke.apply(this.proxy, arguments);
    };
    disconnect = function () {
        this.connection.stop();
    };

    connect = function (queryParams: string) {
        let startOptions: ConnectionOptions = {};
        if (this.options.transport) {
            startOptions.transport = this.options.options.transport;
        }
        if (this.options.jsonp) {
            startOptions.jsonp = this.options.jsonp;
        }
        if (this.options.pingInterval !== undefined) {
            startOptions.pingInterval = this.options.pingInterval;
        }
        if (queryParams) {
            this.connection.qs = queryParams;
        }
        return this.connection.start(startOptions);
    };


}

export interface IHubOptions {
    stateChanged: (change: StateChanged) => void;
    queryParams: string;
    errorHandler: (error: ConnectionError) => void;
    methods: any[];
    listeners: any[];
    logging: boolean;
    rootPath: string;
    useSharedConnection: boolean;
    transport?: string | Array<string> | Transport;
    callback?: Function;
    waitForPageLoad?: boolean;
    jsonp?: boolean;
    pingInterval?: number;
}
