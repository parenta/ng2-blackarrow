export var ContactTypes;
(function (ContactTypes) {
    ContactTypes[ContactTypes["from"] = 0] = "from";
    ContactTypes[ContactTypes["replyTo"] = 1] = "replyTo";
    ContactTypes[ContactTypes["to"] = 2] = "to";
    ContactTypes[ContactTypes["cc"] = 3] = "cc";
    ContactTypes[ContactTypes["bcc"] = 4] = "bcc";
})(ContactTypes || (ContactTypes = {}));
