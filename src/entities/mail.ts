export interface IMail {
    contacts: IMailContact[];
    mailTemplate: string;
    attachments: IMailAttachment[];
    subject: string;
    mergeFields: IMergeField[];
}

export interface IMailContact {
    name: string;
    address: string;
    contactType: ContactTypes;
}

export interface IMailAttachment {
    name: string;
    base64: string;
}

export interface IMergeField {
    name: string;
    value: string;
}

export enum ContactTypes {
    from = 0,
    replyTo = 1,
    to = 2,
    cc = 3,
    bcc = 4
}
