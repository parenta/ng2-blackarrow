import { OpaqueToken } from '@angular/core';

export const BLACKARROW_CLIENT_CONFIG = new OpaqueToken('BLACKARROW_CLIENT_CONFIG');
