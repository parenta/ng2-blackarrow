export interface IBlackArrowClientConfig {
    apiUrl: string;
    isInternal: boolean;
    errorFunc: Function;
}
