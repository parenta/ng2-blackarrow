import {MessengingHubFactoryService} from './messenging-hub-factory.service';
export class NotificationHubService {
    constructor(messagingHubFactory: MessengingHubFactoryService) {
        let hub = messagingHubFactory.createHub('notificationhub', {
            listeners : {
                'NewUserNotification': function(
                    notificationId: any,
                    message: string,
                    action: string,
                    actionType: number,
                    notificationType: number
                ) {
                    $rootScope.$broadcast('NewUserNotification', {
                        notificationId: notificationId,
                        message: message,
                        action: action,
                        actionType: actionType,
                        notificationType: notificationType,
                    });
                }
            },
            queryParams: {
                'token': remoteConfig.getToken()
            },
            rootPath: remoteConfig.getRootPath()
        });
    }
}
