import {Injectable, EventEmitter} from '@angular/core';
import {IHubOptions, MessengingHub} from '../entities/messaging-hub';

class Broadcaster extends EventEmitter {
}

@Injectable()
export class MessengingHubFactoryService {
    constructor(private $: JQueryStatic, private broadcaster: Broadcaster) {
        let hub = new createHub('notificationhub', {
            listeners : {
                'NewUserNotification': function(
                    notificationId: any,
                    message: string,
                    action: string,
                    actionType: number,
                    notificationType: number
                ) {
                    $rootScope.$broadcast('NewUserNotification', {
                        notificationId: notificationId,
                        message: message,
                        action: action,
                        actionType: actionType,
                        notificationType: notificationType,
                    });
                }
            },
            queryParams: {
                'token': 'remoteConfig.getToken()'
            },
            rootPath: 'remoteConfig.getRootPath()'
        });
    }
    createHub(hubName: string, options: IHubOptions): MessengingHub {
        return new MessengingHub(hubName, options, this.$);
    }
}


