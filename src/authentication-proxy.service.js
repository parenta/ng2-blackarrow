import { AuthenticationService } from '@parenta/ng2-sauron';
import { Injectable } from '@angular/core';
export var AuthenticationProxyService = (function () {
    function AuthenticationProxyService(authenticationService) {
        this.authenticationService = authenticationService;
    }
    AuthenticationProxyService.prototype.sauronKey = function () {
        return this.authenticationService.generateAuthKey();
    };
    AuthenticationProxyService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AuthenticationProxyService.ctorParameters = [
        { type: AuthenticationService, },
    ];
    return AuthenticationProxyService;
}());
