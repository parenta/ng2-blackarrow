import { Observable } from 'rxjs';
import { Http, RequestOptions, Headers } from '@angular/http';
import { AuthenticationProxyService } from './authentication-proxy.service';
import { Injectable, Inject } from '@angular/core';
import { BLACKARROW_CLIENT_CONFIG } from './config/black-arrow-client-config';
export var MailService = (function () {
    function MailService(config, http, authenticationProxyService) {
        this.config = config;
        this.http = http;
        this.authenticationProxyService = authenticationProxyService;
    }
    MailService.prototype.sendMail = function (mail) {
        var that = this;
        var headers = new Headers();
        var authKey = this.authenticationProxyService.sauronKey();
        headers.append('Authorization', 'Token ' + authKey);
        var options = new RequestOptions({ headers: headers });
        return this.http.post(this.config.apiUrl + 'mail', mail, options)
            .map(function (res) {
            var body = res.json();
            return body || '';
        })
            .catch(function (error) {
            if (that.config.errorFunc) {
                that.config.errorFunc(error);
            }
            var errMsg = (error.message) ? error.message :
                error.status ? error.status + " - " + error.statusText : 'Server error';
            console.error(errMsg); // log to console instead
            return Observable.throw(errMsg);
        });
    };
    MailService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    MailService.ctorParameters = [
        { type: undefined, decorators: [{ type: Inject, args: [BLACKARROW_CLIENT_CONFIG,] },] },
        { type: Http, },
        { type: AuthenticationProxyService, },
    ];
    return MailService;
}());
