import {IMail} from './entities/mail';
import {Observable} from 'rxjs';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {AuthenticationProxyService} from './authentication-proxy.service';
import {Injectable, Inject} from '@angular/core';
import {IBlackArrowClientConfig} from './config/iblack-arrow-client-config';
import {BLACKARROW_CLIENT_CONFIG} from './config/black-arrow-client-config';

@Injectable()
export class MailService {
    constructor(
        @Inject(BLACKARROW_CLIENT_CONFIG)private config: IBlackArrowClientConfig,
        private http: Http, private authenticationProxyService: AuthenticationProxyService) {

    }

    sendMail(mail: IMail): Observable<string> {
        let that = this;


        let authKey = this.authenticationProxyService.sauronKey();

        let headers = new Headers({
            'Authorization' : 'Token ' + authKey,
            'Content-Type' : 'application/json',
            'Accept': 'q=0.8;application/json;q=0.9'
        });

        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.config.apiUrl + 'mail', mail, options)
            .map(res => {
                let body = res.json();
                return body || '';
            })
            .catch(error => {
                if (that.config.errorFunc) {
                    that.config.errorFunc(error);
                }
                let errMsg = (error.message) ? error.message :
                    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
                console.error(errMsg); // log to console instead
                return Observable.throw(errMsg);
            });
    }
}
