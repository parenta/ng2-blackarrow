#addin "Cake.Json"
#addin "Cake.Npm"
#addin "Cake.Git"

var target = Argument("target", "Default");

Setup(context =>
{
Information(@"               ________          ___.   .__                 __");                                       
Information(@"  ____    ____ \_____  \         \_ |__ |  | _____    ____ |  | _______ ______________  ______  _  __");
Information(@" /    \  / ___\ /  ____/   ______ | __ \|  | \__  \ _/ ___\|  |/ /\__  \\_  __ \_  __ \/  _ \ \/ \/ /");
Information(@"|   |  \/ /_/  >       \  /_____/ | \_\ \  |__/ __ \\  \___|    <  / __ \|  | \/|  | \(  <_> )     / ");
Information(@"|___|  /\___  /\_______ \         |___  /____(____  /\___  >__|_ \(____  /__|   |__|   \____/ \/\_/  ");
Information(@"     \//_____/         \/             \/          \/     \/     \/     \/                            ");
    

});
Task("__Versioning")
    .Does(() => {
		TeamCity.WriteStartBlock("Build number versioning");

		var packageFile = "./package.json";

        var deserializedPackage = ParseJsonFromFile(packageFile);
		TeamCity.SetBuildNumber(deserializedPackage["version"].ToString());

        TeamCity.WriteEndBlock("Build number versioning");
	});

Task("__NpmInstall")
    .Does(() => {
        TeamCity.WriteStartBlock("Npm Install");
        Npm.Install();
        Npm.RunScript("afterinstall");
        TeamCity.WriteEndBlock("Npm Install");
    });

Task("__Build")
    .Does(() => {
        TeamCity.WriteStartBlock("Build");
        Npm.RunScript("beforepublish");
        TeamCity.WriteEndBlock("Build");
    });

Task("__Test")
    .Does(() => {
        TeamCity.WriteStartBlock("Test");
        Npm.RunScript("testme");
        TeamCity.WriteEndBlock("Test");
    });

Task("__Release")
     .Does(() => {
        TeamCity.WriteStartBlock("Commit Release");
        Npm.RunScript("release");
        TeamCity.WriteEndBlock("Commit Release");
     });



Task("Build")
    .IsDependentOn("__Versioning")
    .IsDependentOn("__NpmInstall")
    .IsDependentOn("__Build")
    .IsDependentOn("__Release");


Task("Default")
  .IsDependentOn("Build");

RunTarget(target);